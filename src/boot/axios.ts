// import axios, { AxiosInstance } from 'axios';
// import { boot } from 'quasar/wrappers';

// declare module 'vue/types/vue' {
//   interface Vue {
//     $axios: AxiosInstance;
//   }
// }

// export default boot(({ Vue }) => {
//   // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
//   Vue.prototype.$axios = axios;
// });


import Vue from 'vue'
import axios from 'axios'

Vue.prototype.$axios = axios

const api = axios.create({ baseURL: 'http://172.17.176.1:8881/bvnextgen/api/' })
Vue.prototype.$api = api

export { axios, api }