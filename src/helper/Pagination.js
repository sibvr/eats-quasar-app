export const paginate = (currentPage, perPage, filteredRecords) => {

    const startIndex = perPage * (currentPage - 1);
    const endIndex = startIndex + perPage;

    console.log('start : ' + startIndex + " end:" + endIndex);

    return filteredRecords.slice(startIndex, endIndex);

}

export const paginateWithOrder = (currentPage, perPage, filteredRecords, configOrder) => {

    const startIndex = perPage * (currentPage - 1);
    const endIndex = startIndex + perPage;

    console.log('start : ' + startIndex + " end:" + endIndex);

    if (setOrder && configOrder != null) {

        orderColumn = configOrder.orderColumn;
        orderType = configOrder.orderType;

        //set order and start paginate
        return this._.orderBy(
            filteredRecords,
            orderColumn,
            orderType
        ).slice(startIndex, endIndex);
    } else {
        return filteredRecords.slice(startIndex, endIndex);
    }


}