import Vue from 'vue';
import VueCryptojs from 'vue-cryptojs'

//CryptoJS
Vue.use(VueCryptojs);


const keyps = Vue.CryptoJS.enc.Utf8.parse('4512631236589784');
const ivps = Vue.CryptoJS.enc.Utf8.parse('4512631236589784');

const encryptUsingAES256 =  (plaintext) => {
  console.log("plain : " + plaintext);
 
  var encrypted = Vue.CryptoJS.AES.encrypt(Vue.CryptoJS.enc.Utf8.parse(JSON.stringify(plaintext)), keyps, {
    keySize: 128 / 8,
    iv: ivps,
    mode: Vue.CryptoJS.mode.CBC,
    padding: Vue.CryptoJS.pad.Pkcs7
  });

  console.log("encrypted : " + encrypted);
  return encrypted;
}


const decryptUsingAES256 =  (chipertext) => {
  console.log("chiper : " + chipertext);

    var decrypted = Vue.CryptoJS.AES.decrypt(chipertext, keyps, {
      keySize: 128 / 8,
      iv: ivps,
      mode: Vue.CryptoJS.mode.CBC,
      padding: Vue.CryptoJS.pad.Pkcs7
    });

    console.log('decrypted : ' + decrypted.toString(Vue.CryptoJS.enc.Utf8));
    return decrypted.toString(Vue.CryptoJS.enc.Utf8);

}


export  {
  encryptUsingAES256,
  decryptUsingAES256
}

