import { GetterTree } from 'vuex';
import weeklyTaskModule from '.';
import { StateInterface } from '../index';
import { WeeklyTaskStateInterface } from './state';

const getters: GetterTree<WeeklyTaskStateInterface, StateInterface> = {
  
  weeklyTaskStatus (state:WeeklyTaskStateInterface) {
    return state.status;
  },

  getWeeklyTask (state:WeeklyTaskStateInterface) {
    return state.weeklyTask;
  }
  
};

export default getters;
