import { Module } from 'vuex';
import { StateInterface } from '../index';
import state, { WeeklyTaskStateInterface } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const weeklyTaskModule: Module<WeeklyTaskStateInterface, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state
};

export default weeklyTaskModule;
