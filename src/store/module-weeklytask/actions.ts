import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { WeeklyTaskStateInterface } from './state';
import { api } from 'boot/axios';
import { Getter, namespace } from 'vuex-class';

const actions: ActionTree<WeeklyTaskStateInterface, StateInterface> = {

  async inquiryWeeklyTask ({ commit }, params) {
    
    const headers = {
      'Authorization': 'Bearer ' + params.token
    };

    try {
      //loading
      commit('setWeeklyTaskRequest');

      await api.get('getSumActiveDailyTasByWeekService'+params.params, { headers }).then(function(res) {
        let serviceReturn = res.data;
        console.log('serviceReturn : ' + JSON.stringify(serviceReturn));

        if (serviceReturn['0'].serviceContent.status == 'S') {
          console.log("sukses");

          let dataWeeklyTask = [];
          
          if(serviceReturn['0'].serviceContent.count >0){
            dataWeeklyTask = serviceReturn['0'].serviceContent.serviceOutput;
          }
        
          commit('setWeeklyTask', dataWeeklyTask);
        }else{
          console.log("masuk error 1");
          commit('setWeeklyTaskError');
          return false;
        }

      }).catch(err => {

        console.log("masuk error 2"+err);
        commit('setWeeklyTaskError');
        console.error('fetch failed 1', err);
        return false;
      });

    } catch (err) {
      console.log("masuk error 3"+err);
      commit('setWeeklyTaskError');
      console.error('fetch failed 2', err);
      return false;
    }
   
    return true;
    
  }

};

export default actions;
