export interface WeeklyTaskStateInterface {
  weeklyTask: [],
  status: string
}

function state(): WeeklyTaskStateInterface {
  return {
    weeklyTask: [],
    status: '',
  }
};

export default state;
