import { MutationTree } from 'vuex';
import { WeeklyTaskStateInterface } from './state';

const mutation: MutationTree<WeeklyTaskStateInterface> = {

  setWeeklyTaskRequest (state:WeeklyTaskStateInterface,resp) {
    state.status = 'loading';
  },

  setWeeklyTaskError (state:WeeklyTaskStateInterface,resp) {
    state.status = 'error';
  },

  setWeeklyTask (state:WeeklyTaskStateInterface,resp) {
    state.status = 'success';
    state.weeklyTask = resp;
  },

  
  
};

export default mutation;
