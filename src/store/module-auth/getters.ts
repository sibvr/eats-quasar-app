import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { AuthStateInterface } from './state';

const getters: GetterTree<AuthStateInterface, StateInterface> = {
  
  isAuthenticated(state:AuthStateInterface) {
    return !!state.token;
  },

  authStatus(state:AuthStateInterface) {
    return state.status;
  },

  stateUserid(state:AuthStateInterface) {
    return state.userid;
  },
  
  stateUsername(state:AuthStateInterface) {
    return state.username;
  },

  stateRolename(state:AuthStateInterface) {
    return state.rolename;
  },

  stateUserimage(state:AuthStateInterface) {
    return state.userimage;
  },

  stateRoleTaskList(state:AuthStateInterface) {
    return state.roleTaskList;
  },

  stateToken(state:AuthStateInterface) {
    return state.token;
  },

  stateRefreshToken(state:AuthStateInterface) {
    return state.refreshToken;
  },

  stateUserToken(state:AuthStateInterface) {
    return state.userToken;
  },

};

export default getters;
