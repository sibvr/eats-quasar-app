import { MutationTree } from 'vuex';
import { AuthStateInterface } from './state';

const mutation: MutationTree<AuthStateInterface> = {
 

  setAuthRequest(state:AuthStateInterface) {
    state.status = "loading";
  },

  setAuthSuccess(state:AuthStateInterface, resp) {
      state.status = "success";
      state.userid = resp.userId;
      state.username = resp.username;
      state.rolename = resp.rolenameResult;
      state.userimage = resp.userImageResult;
      state.roleTaskList = resp.roleTaskList;
      state.token = resp.token;
      state.refreshToken = resp.refreshToken;
      state.userToken = resp.userToken;
  },

  setAuthError(state:AuthStateInterface, username) {
      state.status = "error";
      state.userid = "";
      state.username = username;
      state.rolename = "";
      state.userimage = "-";
      state.roleTaskList = [];
      state.token = "";
      state.refreshToken = "";
      state.userToken = "";
  },

  setAuthLogout(state:AuthStateInterface) {
      state.status = "";
      state.userid = "";
      state.username = "";
      state.rolename = "";
      state.userimage = "-";
      state.roleTaskList = [];
      state.token = "";
      state.refreshToken = "";
      state.userToken = "";
  }

};

export default mutation;
