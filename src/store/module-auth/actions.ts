import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { AuthStateInterface } from './state';
import { api } from 'boot/axios';

const actions: ActionTree<AuthStateInterface, StateInterface> = {
  
  async LogIn({ commit }, user) {

    const headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      withCredentials: true
    };

    try {
      //mutation setAuthRequest
      commit('setAuthRequest');

      await api.post('loginService', user, { headers }).then(function(res) {
          let serviceReturn = res.data;
          console.log('serviceReturn : ' + JSON.stringify(serviceReturn));

          if (serviceReturn['0'].serviceContent.status == 'S') {
            console.log("sukses");

            var userCred = {
              username: user.username,
              userId: serviceReturn['0'].serviceContent.id,
              token: serviceReturn['0'].serviceContent.access_token,
              refreshToken: serviceReturn['0'].serviceContent.refresh_token,
              userToken: serviceReturn['0'].serviceContent.user_token
            };

            console.log('userCred : ' + JSON.stringify(userCred));
            //mutation setAuthSuccess

            commit('setAuthSuccess', userCred);
          } else {

            console.log("masuk error 1");
            commit('setAuthError', user.username);
            return false;
          }
        })
        .catch(err => {

          console.log("masuk error 2");
          commit('setAuthError', user.username);
          console.error('fetch failed 1', err);
          return false;
        });
    } catch (err) {

      console.log("masuk error 3");
      commit('setAuthError', user.username);
      console.error('fetch failed 2', err);
      return false;
    }

    return true;
  },

  async LogOut({ commit, getters  }) {

    console.log('token : ' + getters.stateToken);

    try {

        if (getters.stateToken) {

            const headers =
            {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " + getters.stateToken
            };

            await api.post("logoutService", {}, {headers}).then(function (res) {

                var serviceReturn = res.data;
                console.log("serviceReturn : " + JSON.stringify(serviceReturn))

            }).catch(err => {
                console.error('fetch failed 1', err);
            });
        } else {
            
        }
    } catch (err) {
      console.error('fetch failed 2', err);
    }

    commit("setAuthLogout");
  }


};

export default actions;
