export interface AuthStateInterface {
  userid: string,
  username: string,
  rolename:string,
  userimage:string,
  roleTaskList: any[],
  token: string,
  refreshToken: string,
  userToken: string,
  status: string
}

function state(): AuthStateInterface {
  return {
    userid: "",
    username: "",
    rolename: "",
    userimage: "-",
    roleTaskList: [],
    token: "",
    refreshToken: "",
    userToken: "",
    status: ""
  }
};

export default state;
