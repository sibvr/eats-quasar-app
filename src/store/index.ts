import { store } from 'quasar/wrappers';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";
import  authModule  from './module-auth';
import { AuthStateInterface } from './module-auth/state';

import  weeklyTaskModule  from './module-weeklytask';
import { WeeklyTaskStateInterface } from './module-weeklytask/state';

// import example from './module-example';
// import { ExampleStateInterface } from './module-example/state';

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export interface StateInterface {
  // Define your own store structure, using submodules if needed
  // example: ExampleStateInterface;
  // Declared as unknown to avoid linting issue. Best to strongly type as per the line above.
  // example: unknown;
  authModule: AuthStateInterface,
  weeklyTaskModule: WeeklyTaskStateInterface
}

export default store(function ({ Vue }) {
  Vue.use(Vuex);

  const Store = new Vuex.Store<StateInterface>({
    modules: {
      // example
      authModule,
      weeklyTaskModule
    },
    plugins:[
      //save store to local storage
      createPersistedState()
    ],

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: !!process.env.DEBUGGING
  });

  return Store;
});
